use std::{collections::HashMap, fmt::Display};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Chord {
    Maj(Note),
    Min(Note),
    Dim(Note),
    Aug(Note),
    Sus2(Note),
    Sus4(Note),
    Maj7(Note),
    Min7(Note),
    Dim7(Note),
}

impl Default for Chord {
    fn default() -> Self {
        Chord::Maj(Note::C)
    }
}

impl Display for Chord {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Chord::Maj(note) => write!(f, "{note} Maj"),
            Chord::Min(note) => write!(f, "{note} Min"),
            Chord::Dim(note) => write!(f, "{note} Dim"),
            Chord::Aug(note) => write!(f, "{note} Aug"),
            Chord::Sus2(note) => write!(f, "{note} Sus2"),
            Chord::Sus4(note) => write!(f, "{note} Sus4"),
            Chord::Maj7(note) => write!(f, "{note} Maj7"),
            Chord::Min7(note) => write!(f, "{note} Min7"),
            Chord::Dim7(note) => write!(f, "{note} Dim7"),
        }
    }
}

impl Chord {
    fn all() -> Vec<Chord> {
        let mut vec = Vec::<Chord>::default();
        for note in Note::all() {
            vec.push(Chord::Maj(note.clone()));
            vec.push(Chord::Min(note.clone()));
            vec.push(Chord::Dim(note.clone()));
            vec.push(Chord::Aug(note.clone()));
            vec.push(Chord::Sus2(note.clone()));
            vec.push(Chord::Sus4(note.clone()));
            vec.push(Chord::Maj7(note.clone()));
            vec.push(Chord::Min7(note.clone()));
            vec.push(Chord::Dim7(note.clone()));
        }
        return vec;
    }

    /// Obtain the component tones of the chord.
    /// ```rust
    /// # use chord_detector::chord::{Chord, Note};
    /// let chord = Chord::Maj(Note::C);
    /// assert_eq!(
    ///     chord.get_harmonic_set(),
    ///     vec![Note::C, Note::E, Note::G]
    /// )
    /// ```
    pub fn get_harmonic_set(&self) -> Vec<Note> {
        match self {
            Chord::Maj(root) => {
                let root = root.clone();
                let third = root.get_semitones_upper(4);
                let fifth = root.get_semitones_upper(7);
                vec![root, third, fifth]
            }
            Chord::Min(root) => {
                let root = root.clone();
                let third = root.get_semitones_upper(3);
                let fifth = root.get_semitones_upper(7);
                vec![root, third, fifth]
            }
            Chord::Dim(root) => {
                let root = root.clone();
                let third = root.get_semitones_upper(3);
                let fifth = root.get_semitones_upper(6);
                vec![root, third, fifth]
            }
            Chord::Aug(root) => {
                let root = root.clone();
                let third = root.get_semitones_upper(4);
                let fifth = root.get_semitones_upper(8);
                vec![root, third, fifth]
            }
            Chord::Sus2(root) => {
                let root = root.clone();
                let third = root.get_semitones_upper(2);
                let fifth = root.get_semitones_upper(7);
                vec![root, third, fifth]
            }
            Chord::Sus4(root) => {
                let root = root.clone();
                let third = root.get_semitones_upper(5);
                let fifth = root.get_semitones_upper(7);
                vec![root, third, fifth]
            }
            Chord::Maj7(root) => {
                let root = root.clone();
                let third = root.get_semitones_upper(4);
                let fifth = root.get_semitones_upper(7);
                let seventh = root.get_semitones_upper(11);
                vec![root, third, fifth, seventh]
            }
            Chord::Min7(root) => {
                let root = root.clone();
                let third = root.get_semitones_upper(3);
                let fifth = root.get_semitones_upper(7);
                let seventh = root.get_semitones_upper(10);
                vec![root, third, fifth, seventh]
            }
            Chord::Dim7(root) => {
                let root = root.clone();
                let third = root.get_semitones_upper(4);
                let fifth = root.get_semitones_upper(7);
                let seventh = root.get_semitones_upper(10);
                vec![root, third, fifth, seventh]
            }
        }
    }

    fn as_profile(&self) -> Profile {
        let mut profile = [0 as u8; Note::SEMITONES];
        for note in self.get_harmonic_set() {
            profile[note as usize] = 1;
        }
        Profile::new(profile)
    }

    /// Obtain the root tone of the code.
    /// ```rust
    /// # use chord_detector::chord::{Chord, Note};
    /// assert_eq!(Chord::Maj(Note::C).root(), &Note::C)
    /// ```
    pub fn root(&self) -> &Note {
        match self {
            Chord::Maj(root) => root,
            Chord::Min(root) => root,
            Chord::Dim(root) => root,
            Chord::Aug(root) => root,
            Chord::Sus2(root) => root,
            Chord::Sus4(root) => root,
            Chord::Maj7(root) => root,
            Chord::Min7(root) => root,
            Chord::Dim7(root) => root,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Profile([u8; Note::SEMITONES]);

impl Profile {
    fn new(profile: [u8; Note::SEMITONES]) -> Self {
        Self(profile)
    }

    /// https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=472f436ad317b23d7bd1ad53fcb4f23e8f0997a7
    /// eq. 6
    fn score(&self, chroma: &Chroma, bias: f32) -> f32 {
        let sum: f32 = (0..Note::SEMITONES)
            .map(|index| ((1 - self.0[index]) as f32) * (chroma.0[index].powf(2.0)))
            .sum();
        let number_of_tones: u8 = self.0.iter().sum();
        let delta = sum.sqrt() / ((Note::SEMITONES as f32 - number_of_tones as f32) * bias);
        return delta;
    }
}

/// The chord profiles to classify the chord.
#[derive(Debug, PartialEq, Eq)]
pub struct Profiles(HashMap<Chord, Profile>);

impl Profiles {
    pub fn new() -> Self {
        let mut hashmap = HashMap::<Chord, Profile>::default();
        let chords = Chord::all();
        for chord in chords {
            hashmap.insert(chord.clone(), chord.as_profile());
        }
        return Profiles(hashmap);
    }

    /// Classify the chord from the chromagram.
    pub fn classify(&self, chroma: &Chroma) -> Chord {
        let chroma = chroma.suppress_fifth();
        let mut best_matched: (Chord, f32) = (Chord::default(), f32::MAX);
        for (chord, profile) in self.0.iter() {
            let delta = profile.score(&chroma, Profiles::BIAS);
            if delta < best_matched.1 {
                best_matched.0 = chord.clone();
                best_matched.1 = delta;
            }
        }
        return best_matched.0;
    }

    const BIAS: f32 = 1.06;
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
#[repr(u8)]
pub enum Note {
    C,
    Cs,
    D,
    Ds,
    E,
    F,
    Fs,
    G,
    Gs,
    A,
    As,
    B,
}

impl Note {
    pub fn all() -> Vec<Note> {
        (0..Note::SEMITONES).map(|i| Note::from(i as u8)).collect()
    }

    fn get_semitones_upper(&self, semitones: u8) -> Note {
        let index: u8 = self.clone() as u8;
        Note::from(index + semitones)
    }

    /// Number of semitones of in an octave.
    pub const SEMITONES: usize = 12;
}

impl Display for Note {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let note = match self {
            Note::C => "C",
            Note::Cs => "C#",
            Note::D => "D",
            Note::Ds => "D#",
            Note::E => "E",
            Note::F => "F",
            Note::Fs => "F#",
            Note::G => "G",
            Note::Gs => "G#",
            Note::A => "A",
            Note::As => "A#",
            Note::B => "B",
        };
        write!(f, "{note}")
    }
}

impl From<u8> for Note {
    fn from(value: u8) -> Self {
        let index = value % Note::SEMITONES as u8;
        match index {
            0 => Note::C,
            1 => Note::Cs,
            2 => Note::D,
            3 => Note::Ds,
            4 => Note::E,
            5 => Note::F,
            6 => Note::Fs,
            7 => Note::G,
            8 => Note::Gs,
            9 => Note::A,
            10 => Note::As,
            11 => Note::B,
            _ => unreachable!(),
        }
    }
}

/// Represents the chromagram.
#[derive(Debug)]
pub struct Chroma([f32; Note::SEMITONES]);

impl Chroma {
    pub fn new(chroma: [f32; Note::SEMITONES]) -> Self {
        Self(chroma)
    }

    fn suppress_fifth(&self) -> Self {
        let mut chroma = self.0;
        for root in Note::all() {
            let fifth = root.get_semitones_upper(7) as usize;
            let root = root as usize;
            chroma[fifth] -= 0.1 * chroma[root];
            chroma[fifth] = chroma[fifth].clamp(0.0, f32::MAX);
        }
        return Chroma(chroma);
    }
}

#[cfg(test)]
mod tests {
    use crate::chord::{Chord, Chroma, Note, Profile, Profiles};

    #[test]
    fn profile() {
        let profiles = Profiles::new();
        assert_eq!(
            profiles.0.get(&Chord::Maj(Note::C)).unwrap(),
            &Profile::new([1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0])
        );
        assert_eq!(
            profiles.0.get(&Chord::Maj7(Note::C)).unwrap(),
            &Profile::new([1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1])
        );
    }

    #[test]
    fn classify() {
        let profiles = Profiles::new();

        let c_maj = Chroma::new([1.0, 0.1, 0.1, 0.1, 1.0, 0.1, 0.1, 1.0, 0.1, 0.1, 0.1, 0.1]);
        assert_eq!(profiles.classify(&c_maj), Chord::Maj(Note::C));

        let c_maj7 = Chroma::new([1.0, 0.1, 0.1, 0.1, 1.0, 0.1, 0.1, 1.0, 0.1, 0.1, 0.1, 1.0]);
        assert_eq!(profiles.classify(&c_maj7), Chord::Maj7(Note::C));
    }

    #[test]
    fn chord_hash() {
        assert_ne!(Chord::Maj(Note::C), Chord::Min(Note::C));
        assert_ne!(Chord::Maj(Note::C), Chord::Maj(Note::D));
    }
}
