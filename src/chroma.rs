use num_complex::{Complex, ComplexFloat};

use crate::chord::{Chroma, Note};

#[derive(Debug)]
pub struct AudioFrame {
    data: Vec<f64>,
    sampling_freq: f32,
}

impl AudioFrame {
    pub fn new(data: Vec<f64>, sampling_freq: f32) -> Self {
        Self {
            data,
            sampling_freq,
        }
    }

    pub fn len(&self) -> usize {
        self.data.len()
    }

    pub fn downsample(self, divider: usize) -> AudioFrame {
        let mut data = self.data;
        AudioFrame {
            data: data.into_iter().step_by(divider).collect(),
            sampling_freq: self.sampling_freq / divider as f32,
        }
    }
}

#[derive(Debug)]
struct Magnitude {
    inner: Vec<f32>,
    sampling_freq: f32,
}

impl Magnitude {
    pub fn new(value: Vec<Complex<f32>>, sampling_freq: f32) -> Self {
        Magnitude {
            inner: value.into_iter().map(|c| c.abs()).collect(),
            sampling_freq,
        }
    }

    pub fn chromagram(&self) -> Chroma {
        let mut chroma = [0.0; Note::SEMITONES];
        for note in Note::all() {
            let mut oct_sum = f32::default();

            for oct in 1..=Magnitude::NUM_OCT {
                let mut harmonic_sum = f32::default();

                for harmonic in 1..=Magnitude::NUM_HARMONIC {
                    let divider = self.sampling_freq / self.inner.len() as f32;
                    let center_bin =
                        (note.freq() * oct as f32 * harmonic as f32 / divider).round() as usize;
                    let min_bin = center_bin - (Magnitude::R * harmonic);
                    let max_bin = center_bin + (Magnitude::R * harmonic);
                    let mut max = f32::default();
                    for i in min_bin..max_bin {
                        if self.inner[i] > max {
                            max = self.inner[i];
                        }
                    }
                    harmonic_sum += max / harmonic as f32;
                }
                oct_sum += harmonic_sum;
            }
            chroma[note as usize] = oct_sum;
        }
        Chroma::new(chroma)
    }

    const NUM_OCT: usize = 2;
    const NUM_HARMONIC: usize = 2;
    const R: usize = 2;
}

impl Note {
    /// The freqency (Hz) of C1.
    const REF_FREQ: f32 = 130.81278265;

    fn freq(&self) -> f32 {
        let index = self.clone() as u8;
        Note::REF_FREQ * 2.0.powf((index / 12) as f32)
    }
}
